#include "Engine.h"

Engine::Engine(string title){
    SDL_Init(SDL_INIT_VIDEO);
        IMG_Init(IMG_INIT_PNG);
        window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 480, SDL_WINDOW_RESIZABLE);
        renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
};

Engine::~Engine(){
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
    };
void Engine::run(){
        bool running = true;
        SDL_Event event;

        ifstream tileSetFile("resources/tiles/tile_set.txt");
        TileSet *t = new TileSet(tileSetFile, renderer);


        ifstream levelFile ("resources/levels/level1.txt");
        Level *l = new Level(levelFile, t);
        drawables.push_back(l);

        while(running){
            while(SDL_PollEvent(&event)){
                switch(event.type){
                case SDL_QUIT:
                    running = false;
                    break;
                }
            };

            SDL_SetRenderDrawColor(renderer, 0,0,0,255);
            SDL_RenderClear(renderer);
            //l->draw(renderer);
            for (size_t i = 0; i < drawables.size(); i++){
                drawables[i]->draw(renderer);
            }

            SDL_RenderPresent(renderer);

        };
    };
