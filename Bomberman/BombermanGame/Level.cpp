#include "Level.h"

Level::Level(istream &inputStream, TileSet *tileSet){
        this->tileSet = tileSet;

        int rows, cols;
        char code;
        inputStream >> rows >> cols;
        for (int i = 0; i<rows; i++){
            level.push_back(vector<char>());
            for (int j = 0; j<cols; j++ ){
                inputStream >> code;
                level[i].push_back(code);
            }

        }
    };

void Level::draw(SDL_Renderer* renderer) {

        for (size_t i=0; i < level.size(); i++){
            for (size_t j=0; j < level[i].size(); j++){
                if (level[i][j]=='s'){
                        tileSet->draw(level[i][j], i, j, renderer);
                }
                    else{
                tileSet->draw(level[i][j], i*32, (j*32)+48, renderer);
                }
            }
        }

    };
