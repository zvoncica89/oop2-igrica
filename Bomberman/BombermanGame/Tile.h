#ifndef TILE_H_INCLUDED
#define TILE_H_INCLUDED

#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>

using namespace std;


class Tile{
private:
    SDL_Rect *tileRect;
    string passable;
public:
    Tile(int x, int y, int w, int h, string passable){
        tileRect = new SDL_Rect;
        tileRect->x = x;
        tileRect->y = y;
        tileRect->w = w;
        tileRect->h = h;
        this->passable = passable;
    }

    Tile(istream &inputStream){
        tileRect = new SDL_Rect;
        inputStream >> tileRect->x >> tileRect->y >> tileRect->w >> tileRect->h >> this->passable;
    }

    SDL_Rect* getRect(){
        return tileRect;
    }

    int getW(){
        return tileRect->w;
    }
    int getH(){
        return tileRect->h;
    }
};

#endif // TILE_H_INCLUDED

