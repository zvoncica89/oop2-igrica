#ifndef TILESET_H_INCLUDED
#define TILESET_H_INCLUDED

#include <iostream>
#include <map>
#include "Tile.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>

using namespace std;

class TileSet {
private:
    map<char, Tile*> tiles;
    SDL_Texture *texture;
    SDL_Rect* dest;

public:
    TileSet(istream &inputStream, SDL_Renderer* renderer){
      string path;
      inputStream >> path;
      SDL_Surface* surface = IMG_Load(path.c_str());
      texture = SDL_CreateTextureFromSurface(renderer,surface);
      SDL_FreeSurface(surface);

      char code;
      while(!inputStream.eof()){
        inputStream >> code;
        tiles[code] = new Tile(inputStream);
      }

        dest = new SDL_Rect;

    };

    void draw(char code, int x, int y, SDL_Renderer* renderer){
        dest->x = x;
        dest->y = y;
        dest->w = 2*tiles[code]->getW();
        dest->h = 2*tiles[code]->getH();
        //dest->w = tiles[code]->getRect()->w; //napraviti get za tile pravougaonik
        //dest->h = tiles[code]->getRect()->h;
        SDL_RenderCopy(renderer, texture, tiles[code]->getRect(), dest);
    }

};


#endif // TILESET_H_INCLUDED
