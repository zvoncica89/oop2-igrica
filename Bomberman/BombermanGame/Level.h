#ifndef LEVEL_H_INCLUDED
#define LEVEL_H_INCLUDED

#include <iostream>
#include <map>
#include "Tile.h"
#include "TileSet.h"
#include "Drawable.h"
#include <vector>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>

class Level : public Drawable {
private:
    TileSet* tileSet;
    vector<vector<char> > level; //paziti na razmak izmedju >>

public:
    Level(istream &inputStream, TileSet *tileSet);

    virtual void draw(SDL_Renderer* renderer);



};


#endif // LEVEL_H_INCLUDED
