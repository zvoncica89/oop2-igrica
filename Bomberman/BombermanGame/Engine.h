#ifndef ENGINE_H_INCLUDED
#define ENGINE_H_INCLUDED
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <fstream>
#include <vector>
#include "TileSet.h"
#include "Level.h"

using namespace std;


class Engine{
private:
    vector<Drawable*> drawables;
    SDL_Window *window;
    SDL_Renderer *renderer;

public:
    Engine(string title);
    virtual ~Engine();
    void run();


};

#endif // ENGINE_H_INCLUDED
