#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include "Engine.h"
#include "Tile.h"

using namespace std;

int main(int argv, char ** args)
{
    Engine *pEngine = new Engine("Igrica");
    pEngine->run();


    delete pEngine;


    return 0;
}
