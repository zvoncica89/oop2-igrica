#ifndef DRAWABLE_H_INCLUDED
#define DRAWABLE_H_INCLUDED

#include <iostream>
#include <map>
#include "Tile.h"
#include "TileSet.h"
#include <vector>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>

class Drawable{
public:
    virtual void draw(SDL_Renderer* renderer) =0;


};


#endif // DRAWABLE_H_INCLUDED

